<?php require('_header.php'); ?>
<?php
$success_display = 'none';
$countries = array(
  'Andorra',
  'Australia',
  'Austria',
  'Belarus',
  'Belgium',
  'Benelux',
  'Bulgaria',
  'Canada',
  'China',
  'Croatia',
  'Czech Repubic',
  'Estonia',
  'Finland',
  'France',
  'Hong Kong',
  'Italy',
  'Japan',
  'Korea',
  'Liechtenstein',
  'Luxemburg',
  'New Zealand',
  'Norway',
  'Poland',
  'Portugal',
  'Russia',
  'Scotland',
  'Slovak Republic',
  'Slovenia',
  'Spain',
  'Sweden',
  'Switzerland',
  'Turkey',
  'Ukraine',
  'United Kingdom',
  'USA',
  'Other'
);

if ($_POST && isset($_POST['contact_form'])) {
  // country, subject, content, real_person

  if (!empty($_POST['country']) && !empty($_POST['subject']) && !empty($_POST['content']) && !empty($_POST['recaptcha_response_field'])) {

    $_SESSION['contact_form_msg'] = 'Thanks for the contact!';

    $emails = array(
      'Andorra'         => 'a.debalanzo@fholding.es',
      'Australia'       => 'sales@greenski.com.au',
      'Austria'         => 'info@trendsport.co.a',
      'Belarus'         => 'info@hellingski.nl',
      'Belgium'         => 'info@hellingski.nl',
      'Benelux'         => 'info@hellingski.nl',
      'Bulgaria'        => 'info@descente.bg',
      'Canada'          => 'info@descente.com',
      'China'           => 'y-tsukada@descente.com.cn',
      'Croatia'         => 'info@trendsport.co.a',
      'Czech Repubic'   => 'info@voelkl.cz',
      'Estonia'         => 'Info@hawaii.ee',
      'Finland'         => 'post@descente.no',
      'France'          => 'info@clco.it',
      'Hong Kong'       => 'info@descente.com.hk',
      'Italy'           => 'info@clco.it',
      'Japan'           => 'inter@descente.co.jp',
      'Korea'           => 'serena0210@descente.co.kr',
      'Liechtenstein'   => 'sales@textile-trade.ch',
      'Luxemburg'       => 'info@hellingski.nl',
      'New Zealand'     => 'gravity.sports@xtra.co.nz',
      'Norway'          => 'post@descente.no',
      'Poland'          => 'sat@sat-pozan.pl',
      'Portugal'        => 'a.debalanzo@fholding.es',
      'Russia'          => 'info@funsport.ru',
      'Scotland'        => 'info@hellingski.nl',
      'Slovak Republic' => 'info@voelkl.cz',
      'Slovenia'        => 'info@trendsport.co.a',
      'Spain'           => 'a.debalanzo@fholding.es',
      'Sweden'          => 'post@descente.no',
      'Switzerland'     => 'sales@textile-trade.ch',
      'Turkey'          => 'sporart@ttmail.com',
      'Ukraine'         => 'info@funsport.ru',
      'United Kingdom'  => 'info@hellingski.nl',
      'USA'             => 'info@descente.com',
      'Other'           => 'info@descente.com',
      'tech'            => 'aaron@blinkss.com'
    );

    $country = isset($_POST['country']) ? strtoupper($_POST['country']) : '';

    $body = "Contact from http://descente.com visitor:\n\n";
    $body .= "Name: ".$_POST['full-name']."\n";
    $body .= "Country: ".$country."\n";
    $body .= "Email: ".$_POST['email']."\n";
    $body .= "Subject: ".$_POST['subject']."\n";
    $body .= 'Message: '.$_POST['content']."\n";

    $to = 'info@descente.com';
    if (array_key_exists($_POST['country'], $emails)) {
      $to = $emails[$_POST['country']];
    }

    if ($_POST['subject'] == "warranty") {
      if ($_POST['country'] == 'usa') {
        $to = "warrantyusa@descente.com";
      } else if ($_POST['country'] == 'canada') {
        $to = "warrantyca@descente.com";
      }
    }

    if ($_POST['subject'] == "sticker") {
      if ($_POST['country'] == 'usa') {
        $to = "receptionist@descente.com";
      } else if ($_POST['country'] == 'canada') {
        $to = "jenlu@descente.com";
      }
    }

    if ($_POST['subject'] == "online-order") {
      if ($_POST['country'] == 'ca') {
        $to = "Jenlu@descente.com";
      } else {
        $to = "Jeremy@descente.com";
      }
    }

    if (array_key_exists($_POST['country'], $emails) && $_POST['country'] == 'tech') {
      $to = $emails['tech'];
    }

    $subject = "DESCENTE.com Contact Form Submission ($country)";

    $from     = $_POST['email'];
    $reply_to = $_POST['full-name']." <$from>";
    $headers  = "From: <$from>\r\nReply-To: $reply_to\r\n";

    mail($to, $subject, $body, $headers);

    unset($_POST);

    $success_display = 'block';
  }
}
?>
<style type="text/css">
  .error-message {
    border: 1px solid #fbc2c4;
    background: #fbe3e4;
    color: #8a1f11;
    padding: 2px 6px;
    margin: 10px 0 6px 0;
  }

  .success-message {
    background: #e6efc2;
    color: #264409;
    border: 1px solid #c6d880;
    padding: 2px 6px;
    margin: 10px 0 6px 0;
    text-align: center;
  }

  hr{
    border-bottom: 1px solid #e5e5e5;
  }
  .row.divide {
    
  }
  .sidebar {
    border-right: 1px solid #e5e5e5;
  }
  #contact-form {
    padding-left: 10px;
  }
  #contact-form input[type="text"], #contact-form textarea {
    background: white;
    border: none;
    border-left: 3px solid #05AFE9;
    border-radius: 0;
    margin-bottom: 10px;
    width: 100%;
    background: #fafdff;
    padding: 5px;
  }
  #contact-form textarea {
    width: 100%;
    min-height: 200px;
  }
  #contact-form select {
    margin-bottom: 10px;
  }
  #contact-form input[type="submit"] {
    width: 100%;
    margin-bottom: 20px;
    border: none;
    border-radius: 0;
    background: #CBEFFA;
    height: 50px;
    font-size: 16px;
    font-weight: 600;
  }
  #map {
    margin-top: 10px;
  }
  #phone {
    color: #05AFE9;
    font-size: 20px;
    font-family: "Noto Sans", Arial, "Helvetica Neue", Helvetica, sans-serif;
  }
  label[for="phone"] {
    font-style: italic;
    font-weight: normal;
    font-size: 12px;
    font-family: "Noto Sans", Arial, "Helvetica Neue", Helvetica, sans-serif;
    margin-bottom: 0;
    color: #666666;
  }
  h2.blue {
    color: #05AFE9;
    font-weight: normal;
    font-size: 16px;
    margin-top: 0;
    margin-bottom: 15px;
  }

  .select2-container{
    width: 100%;
    margin-bottom: 10px;
  }

  .select2-container .select2-choice{
    background: #e5f7fd !important;
    border: 1px solid #e5f7fd;
  }
  h1{
    font-family: "nudista-web", Arial, "Helvetica Neue", Helvetica, sans-serif;
    font-size: 40px;
    font-weight: 300;
    margin-bottom: 40px;
  }
  h4{
    font-weight: 600;
    font-size: 16px;
    margin: 0;
    padding: 0;
    color: #666666;
  }
  p{
    font-size: 13px;
    font-weight: 200;
    font-family: "Noto Sans", Arial, "Helvetica Neue", Helvetica, sans-serif;
    color: #666666;
    padding-bottom: 10px;
    border-bottom: 1px solid #e5e5e5;
  }
</style>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      $('select').select2({
          minimumResultsForSearch: -1
      });
      console.log('submit');
      var fields = ['country', 'email', 'full-name', 'subject', 'content'];
      $('#contact-form').submit(function() {
        var errors = [];
        for (var i in fields) {
          var id = fields[i];
          if ($('#' + id).val() == '') {
            errors.push(id);
          }
        }
        if (errors.length) {
          for (var j in errors) {
            $('#' + errors[j] + '-error').show();
          }
          return false;
        }
      });
    });
  </script>
  <div id="success-message" class="success-message" style="display: <?php echo $success_display; ?>;">Message sent!</div>
  <div class="row">
    <div class="col-xs-12">
      <h1>We love hearing from you.</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3 sidebar">
      <div class="row divide">
        <div class="col-xs-12">
          <h4>Descente North America</h4>
          <p>
            334 North Marshall Way<br/>
            Layton, UT 84041
          </p>
        </div>
      </div>
      <div class="row divide">
        <div class="col-xs-12">
          <label for="phone">North America Only</label>
          <p id="phone" class="phone">
            (800) 999-0475
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <img id="map" src="images/map.jpg" style="width:196px; height: 196px;" />
        </div>
      </div>
    </div>
    <div class="col-sm-9">
      <form id="contact-form" action="" method="post">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="blue">
              <strong>Send us a message</strong> and we'll respond within 48 hours.
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div id="country-error" class="error-message" style="display: none;">A country is required.</div>
            <select id="country" name="country">
              <option>Please select your Country</option>
              <?php foreach ($countries as $country) : ?>
                <option value="<?= $country; ?>"><?= $country; ?></option>
              <?php endforeach; ?>
              <option value="tech">Tech</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div id="full-name-error" class="error-message" style="display: none;">Please enter your name.</div>
            <input type="text" id="full-name" name="full-name" value="" placeholder="First and Last name"/>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div id="email-error" class="error-message" style="display: none;">Please enter an email.</div>
            <input type="text" id="email" name="email" value="" placeholder="Email Address"/>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div id="subject-error" class="error-message" style="display: none;">Please select a subject.</div>
            <select id="subject" name="subject">
              <option>Subject</option>
              <option value="warranty" <?php echo $_POST['subject'] == 'warranty' ? 'selected' : ''; ?>>Warranty</option>
              <option value="sticker" <?php echo $_POST['subject'] == 'sticker' ? 'selected' : ''; ?>>Sticker Request</option>
              <option value="online-order" <?php echo $_POST['subject'] == 'online-order' ? 'selected' : ''; ?>>Online Order</option>
              <option value="other" <?php echo $_POST['subject'] == 'other' ? 'selected' : ''; ?>>Other</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div id="content-error" class="error-message" style="display: none;">Please enter a message.</div>
            <textarea id="content" name="content" placeholder="Enter your message here..."></textarea>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <input type="checkbox" id="signup" name="signup" value="1" checked="checked"/>
            <label for="signup"></label>
            <label for="signup">Yes, please sign me up for updates and promotions.</label>
          </div>
        </div>
        <div class="row divide">
          <div class="col-xs-12">
            <input type="submit" class="" value="SEND"/>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <p class="privacy">
              <img src="images/heart.jpg" style="width: 12px;" /> We respect <a href="#">your privacy</a>. Your information will never be shared.
            </p>
          </div>
        </div>
      </form>
    </div>
  </div>
