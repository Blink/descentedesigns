jQuery(document).ready(function($) {
  $(document).on('click', '#main-menu .menu-item', function(event) {
    event.preventDefault();
    $('.menu-nav').hide();
    $('.menu-item').removeClass('active');
    $(this).addClass('active');
    $('#'+$(this).data('menu')).show();
  });
});