<html>
  <head>
    <title>Descente.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1"/>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.10.4/css/ui-lightness/jquery-ui-1.10.4.min.css"/>
    <link rel="stylesheet" type="text/css" href="/js/select2-3.4.8/select2.css"/>
    <link rel="stylesheet" type="text/css" href="/js/royalslider/royalslider.css"/>
    <link rel="stylesheet" type="text/css" href="/js/responsive-nav/responsive-nav.css "/>
    <link rel="stylesheet" href="/js/customscroll/jquery.mCustomScrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="/css/main.css"/>
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui-1.10.4/js/jquery-ui-1.10.4.js"></script>
    <script type="text/javascript" src="/js/select2-3.4.8/select2.js"></script>
    <script type="text/javascript" src="/js/royalslider/jquery.royalslider.min.js"></script>
    <script type="text/javascript" src="/js/responsive-nav/responsive-nav.min.js"></script>
    <script src="/js/customscroll/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/js/retina-1.3.0/retina.min.js"></script>
    <script type="text/javascript" src="/js/menu.js"></script>
    <script type="text/javascript" src="//use.typekit.net/vjn6tgu.js"></script>
    <script type="text/javascript">try {
        Typekit.load();
      } catch (e) {
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {

        $("#tabs-min").tabs();

        $(document).on('click', '.search .icon', function() {
          if ($('.search .search-wrapper').hasClass('expanded')) {
            $('.search .search-wrapper').animate({
              width: '32px'
            });
            $('.search .search-wrapper').removeClass('expanded');
          } else {
            $('.search .search-wrapper').animate({
              width: '170px'
            });

            $('.search .search-wrapper').addClass('expanded');

          }


          //$('#search').toggle('slide', { direction: 'right' }, 200, function() {
          //});
        });

        var icons = {
          header: "accordian-category-closed",
          activeHeader: "accordian-category-open"
        };

        $('#tabs').tabs({
          activate: function(event, ui) {
            if (ui.newTab.index() == 1) {
              if ($('#accordian-categories').hasClass('ui-accordion')) {
                $('#accordian-categories').accordion('refresh');
              } else {
                $('#accordian-categories').accordion({
                  collapsible: true,
                  icons: icons,
                  animate: false
                });
              }
            }
          }
        });


        /*$( '#accordian-categories' ).accordion({
         collapsible: true,
         icons: icons,
         animate: false
         });*/
      });
    </script>
  </head>
  <body>
    <div class="content white-back">
      <div class="container">