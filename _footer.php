      </div>
    </div>
    <footer class="top">
      <div class="container">
        <div class="divider"></div>
        <div class="row">
          <div class="col-sm-2 follow">
            <h4>Follow Us</h4>
            <nav class="social">
              <a href="#" class="facebook"></a>
              <a href="#" class="twitter"></a>
              <a href="#" class="instagram"></a>
              <a href="#" class="youtube"></a>
            </nav>
          </div>
          <div class="col-sm-3 customer-service">
            <h4><label for="country">Customer Service</label></h4>
            <form action="" method="post">
              <select id="country" name="country">
                <option>Country Select</option>
                <option value="US">USA</option>
                <option value="CA">Canada</option>
              </select>
            </form>
          </div>
          <div class="col-sm-5 newsletter">
            <h4><label for="email">Newsletter Sign Up</label></h4>
            <form action="" method="post">
              <div class="row">
                <div class="col-md-8">
                  <input type="email" id="email" name="email" value="" placeholder="email@example.com"/>
                </div>
                <div class="col-md-4 subscribe">
                  <input type="submit" class="btn btn-gray" value="Subscribe"/>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <nav class="links">
              <a href="#" class="warranty">Warranty</a>
              <a href="#" class="passport">Passport Activation</a>
              <a href="#" class="service">Customer Service</a>
              <a href="#" class="story">The Descente Story</a>
              <a href="#" class="brand">Brand Resources</a>
            </nav>
          </div>
        </div>
      </div>
    </footer>
    <footer class="bottom">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <nav class="legal">
              <span class="copyright">&copy; Copyright 2015 Descente North America</span>
              <a href="#">Privacy</a> |
              <a href="#">Legal Notices</a>
            </nav>
          </div>
        </div>
      </div>
    </footer>
    <footer class="mobile" style="display: none;">
      <div class="container">
        <div class="row">
          <div class="col-xs-6">
            <h4>Stay Connected</h4>
          </div>
          <div class="col-xs-6">
            <nav class="social">
              <a href="#" class="facebook"></a>
              <a href="#" class="twitter"></a>
              <a href="#" class="instagram"></a>
              <a href="#" class="youtube"></a>
            </nav>
          </div>
        </div>
        <div class="row newsletter">
          <div class="col-xs-12">
            <h4><label for="mobile-email">Newsletter Sign Up</label></h4>
          </div>
        </div>
        <form action="" method="post">
          <div class="row newsletter">
            <div class="col-xs-12 input-container">
              <input type="email" id="mobile-email" name="email" value="" placeholder="email@example.com"/>
            </div>
          </div>
          <div class="row newsletter">
            <div class="col-xs-12">
              <input type="submit" class="btn btn-gray" value="Subscribe"/>
            </div>
          </div>
        </form>
        <div class="row">
          <div class="col-xs-12">
            <nav class="links">
              <a href="#">Find a Retailer</a>
              <a href="#">My Account</a>
              <a href="#">FAQs</a>
              <a href="#">Passport Activation</a>
              <a href="#">Customer Service</a>
              <a href="#">Returns</a>
              <a href="#">Warranty</a>
              <a href="#">Brand Resources</a>
              <a href="#">Privacy Policy</a>
              <a href="#">The Descente Story</a>
              <a href="#">Terms of Use</a>
              <a href="#">Feedback</a>
            </nav>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>